﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace ControlReferences
{
    public partial class MainPage : ContentPage
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="T:ControlReferences.MainPage"/> class.
        /// </summary>
        /// 
        public MainPage()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);

        }

        void ViewAllButtonClicked(object sender, System.EventArgs e)
        {
            Navigation.PushAsync(new ViewAllPage());
        }

        void CreateButtonClicked(object sender, System.EventArgs e)
        {
            Navigation.PushAsync(new CreatePage());
        }
    }
}
