﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace ControlReferences
{
    public partial class ViewAllPage : ContentPage
    {
       
        public ViewAllPage()
        {
            InitializeComponent();

            ViewALlEntry.ItemsSource = CreatePage.UserCollection; //itemsource
        }

   
        //void UserEntrySelected(object sender, Xamarin.Forms.SelectedItemChangedEventArgs e)
        //{
        //    User user = (User)((ListView)sender).SelectedItem; //typecast (typetocast)

        //    Navigation.PushAsync(new UserPage(user));
        //}

        void UserEntry_ItemTapped(object sender, Xamarin.Forms.ItemTappedEventArgs e)
        {
            User user = (User)((ListView)sender).SelectedItem; //typecast (typetocast)

            Navigation.PushAsync(new UserPage(user));
        }
    }
    //public class User
    //{
    //    public string Name { get; set; }
    //    public string Age { get; set; }
    //    public string Address { get; set; }
    //    public string Image { get; set; }
    //    public string Bio { get; set; }
    //}

}
 