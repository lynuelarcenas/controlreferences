﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace ControlReferences
{
    public partial class UserPage : ContentPage
    {
       
        public UserPage(User user)
        {
            BindingContext = user;
            InitializeComponent();
        }
    }
}
