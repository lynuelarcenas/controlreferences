﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

using Xamarin.Forms;

namespace ControlReferences
{
    public partial class CreatePage : ContentPage
    {
        
        public static ObservableCollection<User> UserCollection = new ObservableCollection<User>();

        public CreatePage()
        {
            InitializeComponent();

        }

        void SaveButtonClicked(object sender, System.EventArgs e)
        {
            User UserEntry = new User() { Name = NameEntry.Text, Age = AgeEntry.Text, Address = AddressEntry.Text, Image = ImageEntry.Text, Bio = BioEntry.Text };
            UserCollection.Add(UserEntry);
            DisplayAlert("Saved", "New user added.", "Okay");
            //Navigation.PopAsync();


            NameEntry.Text = "";
            AgeEntry.Text = "";
            AddressEntry.Text = "";
            ImageEntry.Text = "";
            BioEntry.Text = "";
        }
    }
}
