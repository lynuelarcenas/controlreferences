﻿using System;

namespace ControlReferences
{
    public class User
    {
        public User()
        {
        }
        public string Name { get; set; }
        public string Age { get; set; }
        public string Address { get; set; }
        public string Image { get; set; }
        public string Bio { get; set; }
    }
}
